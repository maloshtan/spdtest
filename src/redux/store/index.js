// @flow
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
// import { createLogger } from 'redux-logger';
import rootReducer from '../reducers';

const configureStore = () => {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const middlewares = [thunk, promise];
  
  // if (process.env.NODE_ENV !== 'production') {
  //   middlewares.push(createLogger());
  // }
  
  return createStore(
    rootReducer,
    composeEnhancers(
      applyMiddleware(...middlewares)
    )
  );
}

export default configureStore;