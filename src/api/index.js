// @flow
import * as firebase from 'firebase';

var config = {
  apiKey: "AIzaSyDA9i6HKGBxuaSTcNcpmlITQAOmJEeBUA4",
  authDomain: "spdtest-763ae.firebaseapp.com",
  databaseURL: "https://spdtest-763ae.firebaseio.com/",
  storageBucket: "bucket.appspot.com"
};

firebase.initializeApp(config);

// Get a reference to the database service
let dbRef = firebase.database();
let dbOfficesRef = dbRef.ref().child('offices');
let dbArchiveRef = dbRef.ref().child('archive');

export type AddressType = {|
  country: string,
  stateProvince: string,
  postalCode: string,
  city: string,
  streetAddress1: string,
  streetAddress2: string,
|};

export type ContactsType = {|
  phone: string,
  fax: string,
  email: string
|};

export type OfficeType = {|
  id: string,
  address: AddressType,
  contacts: ContactsType,
  primaryHQ: boolean,
  isEdited: boolean,
  dummy?: boolean
|};

const createDummyOffice = (): OfficeType => {
  return {
    id: '',
    address: {
      country: '',
      stateProvince: '',
      postalCode: '',
      city: '',
      streetAddress1: '',
      streetAddress2: '',
    },
    contacts: {
      phone: '',
      fax: '',
      email: ''
    },
    primaryHQ: false,
    isEdited: true,
    dummy: true
  }
};



export const fetchOffices = () =>
  dbOfficesRef.once('value').then(fetchedOffices => {
    const response = Object.entries(fetchedOffices.val()).map(entry =>
      ({
        ...entry[1],
        id: entry[0]
      })
    );

    return response;
  });

export const addOffice = () =>
  dbOfficesRef.push().then(pushResponse => {
    const newOffice = {
      ...createDummyOffice(),
      id: pushResponse.key
    }

    return newOffice;
  });

export const removeOffice = (id: string, reason: string, note: ?string) => {
  dbOfficesRef.child(id).remove();
  return dbArchiveRef.child(id).set({
    reason,
    note
  }).then(() => (id));
}

export const saveEditing = (id: string, office: OfficeType) =>
  dbOfficesRef.child(id).set(office).then(() => (office));