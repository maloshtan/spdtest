// @flow
import * as React from 'react';
import { Button, Grid, Row, Col, Glyphicon } from 'react-bootstrap';
import OfficeEditorContainer from '../OfficeEditorContainer';
import OfficeList from '../OfficeList';
import FetchError from '../FetchError';
import LoadingIndicator from '../LoadingIndicator';
import type { OfficeType } from '../../api';
import typeof { addOffice as AddOffice, fetchOffices as FetchOffices} from '../../redux/actions';

import './OfficesInfo.css';

type Props = {
  officeToEdit: OfficeType,
  offices: Array<OfficeType>,
  isFetching: boolean,
  errorMessage: string,
  onRetry: FetchOffices,
  addOffice: AddOffice,
}

class OfficesInfo extends React.Component<Props> {
  render() {
    const { officeToEdit, offices, isFetching, errorMessage, onRetry, addOffice } = this.props;
    return (
      <div className="OfficesInfo">
        <h1>OFFICES <span>| COMPANY INFO</span></h1>
        <p>
          Updating your location and contact information helps you appeal to
          regional investors and service providers.
        </p>
        <hr />
        <Grid fluid>
          <Row className="show-grid no-gutters">
            <Col xs={6} >
              <div className="equalHeight">
                <Button onClick={addOffice} className="add-office">
                  Add New Office
                </Button>
              </div>
            </Col>
            <Col className="right" xs={6} >
              <div className="equalHeight">
                <p>{
                  isFetching && !offices.length ?
                    <LoadingIndicator height={30} /> :
                    `${offices.length} Offices`
                }</p>
              </div>
            </Col>
          </Row>
        </Grid>
        <div>
          {
            errorMessage ?
              <FetchError message={errorMessage} onRetry={() => onRetry()} /> :
              null
          }
          {
            officeToEdit ?
              <OfficeEditorContainer /> :
              null
          }
          {
            isFetching && !offices.length ?
              <LoadingIndicator height={60} /> :
              <OfficeList offices={offices} />
          }
        </div>
        <hr />
        <Grid fluid>
          <Row className="show-grid no-gutters">
            <Col sm={2} md={2} lg={2} >
              <div className="equalHeight">
                <Button className="step">Back</Button>
              </div>
            </Col>
            <Col sm={4} md={4} lg={3} >
              <div className="equalHeight">
                <p><Glyphicon glyph="plus-sign" /> Provide additional comments</p>
              </div>
            </Col>
            <Col className="right" sm={6} md={6} lg={7} >
              <div className="equalHeight">
                <Button className="step">Skip this step</Button>
                <Button bsStyle="primary">Continue</Button>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default OfficesInfo;