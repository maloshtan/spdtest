// @flow
import * as React from 'react';
import { Col, Form, FormGroup, FormControl } from 'react-bootstrap';
import Select from 'react-select';
import type { AddressType } from '../../api';

type Props = {
  address: AddressType,
  onChange: Event => void,
  getLabel: (string) => string,
};

const AddressFormGroup = ({
  address,
  onChange,
  getLabel
}: Props) => (
    <Form horizontal>
      <FormGroup className="no-gutters">
        <Col className="right" sm={5}>
          <p>{getLabel('country')}</p>
        </Col>
        <Col sm={5}>
          <Select
            name="country"
            value={address['country']}
            onChange={onChange}
            clearable={false}
            options={[
              { value: 'Australia', label: 'Australia', name: 'country' },
              { value: 'Belarus', label: 'Belarus', name: 'country' },
              { value: 'Ukraine', label: 'Ukraine', name: 'country' },
              { value: 'United States', label: 'United States', name: 'country' },
            ]}
          />
        </Col>
      </FormGroup>
      <FormGroup className="no-gutters">
        <Col className="right" sm={5}>
          <p>{getLabel('stateProvince')}</p>
        </Col>
        <Col sm={5}>
          <Select
            name="stateProvince"
            value={address['stateProvince']}
            onChange={onChange}
            clearable={false}
            options={[
              { value: 'California', label: 'California', name: 'stateProvince' },
              { value: 'Washington', label: 'Washington', name: 'stateProvince' },
              { value: 'Ohio', label: 'Ohio', name: 'stateProvince' },
              { value: 'South Dacota', label: 'South Dacota', name: 'stateProvince' },
            ]}
          />
        </Col>
      </FormGroup>
      <FormGroup className="no-gutters">
        <Col className="right" sm={5}>
          <p>{getLabel('postalCode')}</p>
        </Col>
        <Col sm={5}>
          <FormControl
            type="text"
            value={address['postalCode']}
            name={'postalCode'}
            onChange={onChange}
          />
        </Col>
      </FormGroup>
      <FormGroup className="no-gutters">
        <Col className="right" sm={5}>
          <p>{getLabel('city')}</p>
        </Col>
        <Col sm={5}>
          <Select
            name="city"
            value={address['city']}
            onChange={onChange}
            clearable={false}
            options={[
              { value: 'Los Angeles', label: 'Los Angeles', name: 'city' },
              { value: 'Seattle', label: 'Seattle', name: 'city' },
              { value: 'Takoma', label: 'Takoma', name: 'city' },
              { value: 'Chicago', label: 'Chicago', name: 'city' },
            ]}
          />
        </Col>
      </FormGroup>
      <FormGroup className="no-gutters">
        <Col className="right" sm={5}>
          <p>{getLabel('streetAddress1')}</p>
        </Col>
        <Col sm={5}>
          <FormControl
            type="text"
            value={address['streetAddress1']}
            name={'streetAddress1'}
            onChange={onChange}
          />
        </Col>
      </FormGroup>
      <FormGroup className="no-gutters">
        <Col className="right" sm={5}>
          <p>{getLabel('streetAddress2')}</p>
        </Col>
        <Col sm={5}>
          <FormControl
            type="text"
            value={address['streetAddress2']}
            name={'streetAddress2'}
            onChange={onChange}
          />
        </Col>
      </FormGroup>
    </Form>
  );



export default AddressFormGroup;