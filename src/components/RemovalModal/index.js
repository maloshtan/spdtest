// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Button, Modal, Form, FormControl } from 'react-bootstrap';
import Select from 'react-select';
import { removeOffice } from '../../redux/actions';
import typeof { removeOffice as RemoveOffice } from '../../redux/actions';

import './RemovalModal.css';

type Props = {
  id: string,
  show: boolean,
  onHide: void => void,
  removeOffice: RemoveOffice,
};

type State = {
  reason: string,
  note: string,
};

class RemovalModal extends React.Component<Props, State> {
  handleChange: (Event | {label: string, value: string, name?: string}) => void;

  constructor(props) {
    super(props);
    this.state = {
      reason: 'Former Record',
      note: ''
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    console.log(e);
    // condition to account for custom shape of event emmited by Select
    if (e.hasOwnProperty('target') && e.target.tagName === 'TEXTAREA') {
      this.setState({ note: e.target.value });
      return;
    }
    this.setState({ reason: e.value });
  }

  render() {
    const { id, show, onHide, removeOffice } = this.props;
    return (
      <Modal
        show={show} onHide={onHide}
        className="RemovalModal" style={{display: 'flex'}}
        dialogClassName="modal-width"
      >
        <Modal.Body>
          <Grid fluid>
              <Row className="show-grid no-gutters">
                <Col xs={12} sm={12} md={12}>
                  <p>Please tell us why you’re removing this record.</p>
                  <Form>
                    <Select
                      name="reason"
                      value={this.state.reason}
                      onChange={this.handleChange}
                      clearable={false}
                      options={[
                        { value: 'Former Record', label: 'Former Record' },
                        { value: 'Duplicate Record', label: 'Duplicate Record' },
                        { value: 'Record never existed', label: 'Record never existed' },
                        { value: 'Other', label: 'Other' },
                      ]}
                    />
                    <p>Notes:</p>
                    <FormControl
                      value={this.setState.note}
                      componentClass="textarea"
                      onChange={this.handleChange}
                    />
                  </Form>
                </Col>
              </Row>
              <Row className="show-grid no-gutters">
                <Col xs={6} sm={6} md={6}>
                  <Button onClick={onHide} className="step">Cancel</Button>
                </Col>
                <Col xs={6} sm={6} md={6} className="right">
                  <Button
                    bsStyle="primary"
                    onClick={() => {
                      removeOffice(id, this.state.reason, this.state.note);
                      onHide()
                    }}
                  >Remove Record</Button>
                </Col>
              </Row>
            </Grid>

        </Modal.Body>
      </Modal>
    );
  }
}

export default connect(
  null,
  {
    removeOffice
  }
)(RemovalModal);