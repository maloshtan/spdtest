// @flow
import * as React from 'react';
import { Route } from 'react-router';
import { Grid, Row, Col } from 'react-bootstrap';
import Sidebar from '../Sidebar';
import OfficesInfoContainer from '../OfficesInfoContainer';

import './InfoEditor.css';

const InfoEditor = () => (
  <Grid className="InfoEditor" fluid>
    <Row className="show-grid no-gutters sidebar-col">
      <Col xs={12} sm={3} md={2}>
        <Route exact path='/editor/:step?' component={Sidebar} />
      </Col>
      <Col xs={12} sm={9} md={10}>
        <Route exact path='/editor/offices' component={OfficesInfoContainer} />
      </Col>
    </Row>
  </Grid>
);

export default InfoEditor;