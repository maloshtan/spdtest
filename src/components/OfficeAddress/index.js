// @flow
import * as React from 'react';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';

type Props = {
  streetAddress1: string,
  streetAddress2: string,
  city: string,
  stateProvince: string,
  postalCode: string,
  country: string,
  hq: boolean,
}

const OfficeAddress = ({
  streetAddress1,
  streetAddress2,
  city,
  stateProvince,
  postalCode,
  country,
  hq
}: Props) => (
    <Grid fluid>
      <Row className="show-grid no-gutters">
        <Col xs={6} sm={5} md={4} className="right">
          <p>Address:</p>
        </Col>
        <Col xs={6} sm={7} md={8}>
          {hq ? <p className="hq"><Glyphicon glyph="ok" /> Primary HQ</p> : null}
          <p>{streetAddress1}</p>
          <p>{streetAddress2}</p>
          <p>{city}, {stateProvince} {postalCode}</p>
          <p>{country}</p>
        </Col>
      </Row>
    </Grid>
);

export default OfficeAddress;