// @flow
import * as React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import configureStore from './redux/store';

import 'react-select/dist/react-select.css';
import './index.css';

import App from './layout/App/App';

const store = configureStore();


const root = document.getElementById('root');
if(!root){
  throw new Error('root is not presented in the DOM');
}
ReactDOM.render(
  <Provider store={store}>
    <Router history={createBrowserHistory()}>
      <App />
    </Router>
  </Provider>,
  root
);
