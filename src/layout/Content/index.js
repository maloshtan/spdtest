// @flow
import * as React from 'react';
import { Route } from 'react-router';
import InfoEditor from '../../components/InfoEditor';

import './Content.css';

const Content = () => (
  <div className="Content">
    <Route path='/editor' component={InfoEditor} />
  </div>
);

export default Content;